package bean;

public class Fingerprint_template {

	private byte[] fingerprint_template;
	private String fingerprint_template_id;
	public byte[] getFingerprint_template() {
		return fingerprint_template;
	}
	public void setFingerprint_template(byte[] fingerprint_template) {
		this.fingerprint_template = fingerprint_template;
	}
	public String getFingerprint_template_id() {
		return fingerprint_template_id;
	}
	public void setFingerprint_template_id(String fingerprint_template_id) {
		this.fingerprint_template_id = fingerprint_template_id;
	}
}

package bean;

import java.sql.Timestamp;

public class User_inf {
	private String user_id;
	private String user_account;
	private String user_password;
	private String user_nickname;
	private String user_photo;
	private String user_fingerprint;
	private String user_phone;
	private String user_email;
	private String user_department;
	private Timestamp user_register_date;
	private int clock_jurisdiction;
	private int door_jurisdiction;
	private int user_disable;
	private byte[] fingerprint_template;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public byte[] getFingerprint_template() {
		return fingerprint_template;
	}
	public void setFingerprint_template(byte[] fingerprint_template) {
		this.fingerprint_template = fingerprint_template;
	}
	public String getUser_account() {
		return user_account;
	}
	public void setUser_account(String user_account) {
		this.user_account = user_account;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_nickname() {
		return user_nickname;
	}
	public void setUser_nickname(String user_nickname) {
		this.user_nickname = user_nickname;
	}
	public String getUser_photo() {
		return user_photo;
	}
	public void setUser_photo(String user_photo) {
		this.user_photo = user_photo;
	}
	public String getUser_fingerprint() {
		return user_fingerprint;
	}
	public void setUser_fingerprint(String user_fingerprint) {
		this.user_fingerprint = user_fingerprint;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_department() {
		return user_department;
	}
	public void setUser_department(String user_department) {
		this.user_department = user_department;
	}
	public Timestamp getUser_register_date() {
		return user_register_date;
	}
	public void setUser_register_date(Timestamp user_register_date) {
		this.user_register_date = user_register_date;
	}
	public int getClock_jurisdiction() {
		return clock_jurisdiction;
	}
	public void setClock_jurisdiction(int clock_jurisdiction) {
		this.clock_jurisdiction = clock_jurisdiction;
	}
	public int getDoor_jurisdiction() {
		return door_jurisdiction;
	}
	public void setDoor_jurisdiction(int door_jurisdiction) {
		this.door_jurisdiction = door_jurisdiction;
	}
	public int getUser_disable() {
		return user_disable;
	}
	public void setUser_disable(int user_disable) {
		this.user_disable = user_disable;
	}
}

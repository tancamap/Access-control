package bean;

public class Admin_inf {

	private String admin_account;    //管理员账户
	private String admin_password;    //管理员密码
	private String admin_nickname;    //管理员昵称
	private int super_jurisdiction;    //超级权限
	private int user_management_jurisdiction;    //用户管理权限
	private int device_management_jurisdiction;   //设备管理权限
	private int basic_jurisdiction;     //基础权限
	
	public String getAdmin_account() {
		return admin_account;
	}
	public void setAdmin_account(String admin_account) {
		this.admin_account = admin_account;
	}
	public String getAdmin_password() {
		return admin_password;
	}
	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}
	public String getAdmin_nickname() {
		return admin_nickname;
	}
	public void setAdmin_nickname(String admin_nickname) {
		this.admin_nickname = admin_nickname;
	}
	public int getSuper_jurisdiction() {
		return super_jurisdiction;
	}
	public void setSuper_jurisdiction(int super_jurisdiction) {
		this.super_jurisdiction = super_jurisdiction;
	}
	public int getUser_management_jurisdiction() {
		return user_management_jurisdiction;
	}
	public void setUser_management_jurisdiction(int user_management_jurisdiction) {
		this.user_management_jurisdiction = user_management_jurisdiction;
	}
	public int getDevice_management_jurisdiction() {
		return device_management_jurisdiction;
	}
	public void setDevice_management_jurisdiction(int device_management_jurisdiction) {
		this.device_management_jurisdiction = device_management_jurisdiction;
	}
	public int getBasic_jurisdiction() {
		return basic_jurisdiction;
	}
	public void setBasic_jurisdiction(int basic_jurisdiction) {
		this.basic_jurisdiction = basic_jurisdiction;
	}
	
	
}

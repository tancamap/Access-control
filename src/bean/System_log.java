package bean;

import java.sql.Timestamp;

public class System_log {

	private String log_id;
	private Timestamp date;
	private int pass_tf;
	private String reason;
	private String user_account;
	private String user_name;
	
	
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getPass_tf() {
		return pass_tf;
	}
	public void setPass_tf(int pass_tf) {
		this.pass_tf = pass_tf;
	}
	public String getUser_account() {
		return user_account;
	}
	public void setUser_account(String user_account) {
		this.user_account = user_account;
	}
	public String getLog_id() {
		return log_id;
	}
	public void setLog_id(String log_id) {
		this.log_id = log_id;
	}

	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date2) {
		this.date = date2;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}

package web.init;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import DB.DB_connection_pool;
import bean.Fingerprint_template;
import bean.Fingerprint_templates;


@WebServlet(urlPatterns={"/Fingerprint_templates_DB_init"}, loadOnStartup=1)
public class Fingerprint_templates_DB_init extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		String select_sql = "select * from user_data";
	    List<Fingerprint_template> fingerprint_templates  = new ArrayList<Fingerprint_template>();
	    
	    try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_select= conn.prepareStatement(select_sql);
			ResultSet rs = ps_select.executeQuery();
			while(rs.next()){
				Fingerprint_template fingerprint_template = new Fingerprint_template();
				fingerprint_template.setFingerprint_template_id(rs.getString("user_id"));
				fingerprint_template.setFingerprint_template(rs.getBytes("fingerprint_template"));
				fingerprint_templates.add(fingerprint_template);
			}
			Fingerprint_templates.setFingerprint_templates_DB(fingerprint_templates);
			
			rs.close();
			ps_select.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	}


}

package web.init;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import equipment.finger.Finger_tool.CLibrary;

/**
 * Servlet implementation class Finger_init
 */
@WebServlet(urlPatterns={"/Finger_init"}, loadOnStartup=1)
public class Finger_init extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	public void init(ServletConfig config) throws ServletException {
		int ret = CLibrary.INSTANCE.sensorInit();
		if(ret != 0){
    		System.out.println("设备初始化失败！");
    	}
		int sensoropen = CLibrary.INSTANCE.sensorOpen(0);
		if(sensoropen == 0){
    		CLibrary.INSTANCE.sensorFree();
    		System.out.println("链接设备失败！");
    	}
		System.out.println("指纹设备初始化成功");
		config.getServletContext().setAttribute("sensoropen", sensoropen);
	}

}

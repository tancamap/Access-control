package DB;

//import java.beans.PropertyVetoException;
import java.net.URISyntaxException;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

//import com.mchange.v2.c3p0.ComboPooledDataSource;

import Tool.Properties_tool;

public class DB_connection_pool {
	
	
	
	
	private static Properties_tool pp = new Properties_tool(get_url());
	//public static ComboPooledDataSource data_pool = getcpds();    //cp30
	
	public static BasicDataSource data_pool = getcpds();           //DBCP
	
	private static String get_url(){      //获取文件路径
		String url = "";
		try {
			url = DB_connection_pool.class.getResource("/DB/DB_set.properties").toURI().getPath();     //获取路劲(!!!!注意：第一个“/”代表classes根目录)
			//System.out.println(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			System.out.println("获取数据库配置文件失败！");
			e.printStackTrace();
		}
		return url;
	}

	
	
	
	private static BasicDataSource getcpds(){
		BasicDataSource cpds = new BasicDataSource();
		cpds.setDriverClassName(pp.getvalue("Driver"));
		cpds.setUrl(pp.getvalue("URL1")+pp.getvalue("IP")+":"+pp.getvalue("Port")+"/"+pp.getvalue("DB_name"));
		cpds.setUsername(pp.getvalue("DB_user"));
		cpds.setPassword(pp.getvalue("DB_password"));
		
		cpds.setInitialSize(Integer.parseInt(pp.getvalue("Init")));    //设置连接池中初始化连接数量
		cpds.setMinIdle(Integer.parseInt(pp.getvalue("MIN_Link")));   //设置连接池中最小连接数量
		cpds.setMaxIdle(Integer.parseInt(pp.getvalue("MAX_Link")));    //设置连接池中最大的连接数量
		cpds.setMaxOpenPreparedStatements(Integer.parseInt(pp.getvalue("Maxstate")));    //设置连接池中缓存连接数量
		cpds.setMaxTotal(Integer.parseInt(pp.getvalue("maxtotal")));       //设置最大活动链接
		
		return cpds;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*static private ComboPooledDataSource getcpds(){		//获取数据库连接池对象,用于对data_pool的初始化
		ComboPooledDataSource cpds;
		cpds = new ComboPooledDataSource();
		try{
		cpds.setDriverClass(pp.getvalue("Driver")); //loads the jdbc driver
		//cpds.setDriverClass("com.mysql.jdbc.Driver"); //loads the jdbc driver
		}
		catch(PropertyVetoException e){
			System.out.println("创建数据库连接池失败！");
			e.printStackTrace();
		}
		cpds.setJdbcUrl(pp.getvalue("URL1")+pp.getvalue("IP")+":"+pp.getvalue("Port")+"/"+pp.getvalue("DB_name"));
		cpds.setUser(pp.getvalue("DB_user"));
		cpds.setPassword(pp.getvalue("DB_password"));
		cpds.setMaxPoolSize(Integer.parseInt(pp.getvalue("MAX_Link")));       //设置连接池中最大的连接数量
		cpds.setMinPoolSize(Integer.parseInt(pp.getvalue("MIN_Link")));        //设置连接池中最小连接数量
		cpds.setInitialPoolSize(Integer.parseInt(pp.getvalue("Init")));        //设置连接池中初始化连接数量
		cpds.setMaxStatements(Integer.parseInt(pp.getvalue("Maxstate")));        //设置连接池中缓存连接数量
		cpds.setAcquireIncrement(Integer.parseInt(pp.getvalue("AcquireIncrement")));   //设置当链接不够时一次获得的链接数
		
		return cpds;
	}*/
	
}

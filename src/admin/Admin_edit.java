package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import DB.DB_connection_pool;

public class Admin_edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();

		if((int)(request.getSession().getAttribute("super_jurisdiction")) == 1){
			
			String old_account = request.getParameter("old_account");   //原账户
			String admin_account = request.getParameter("admin_account");    //管理员账户
			String admin_nickname = request.getParameter("admin_nickname");   //管理员昵称
			int super_jurisdiction = Integer.parseInt(request.getParameter("super_jurisdiction")); //超级权限
			int user_management_jurisdiction = Integer.parseInt(request.getParameter("user_management_jurisdiction"));   //用户管理权限
			int device_management_jurisdiction = Integer.parseInt(request.getParameter("device_management_jurisdiction"));   //设备管理权限
			int basic_jurisdiction = Integer.parseInt(request.getParameter("basic_jurisdiction"));   //基础权限
			
			String update_sql = "update admin_login set admin_account = '"+admin_account+"' , admin_nickname = '"+admin_nickname+"' , super_jurisdiction = '"+super_jurisdiction+"' , user_management_jurisdiction = '"+user_management_jurisdiction+"' , device_management_jurisdiction = '"+device_management_jurisdiction+"' , basic_jurisdiction = '"+basic_jurisdiction+"' where admin_account = '"+old_account+"'";
			
			try {
				Connection conn = DB_connection_pool.data_pool.getConnection();
				PreparedStatement ps_update = conn.prepareStatement(update_sql);
				ps_update.executeUpdate();
				
				ps_update.close();
				conn.close();
				jsonObj.put("result", true);
			} catch (SQLException e) {
				jsonObj.put("result", false);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			jsonObj.put("result", false);
		}
		out.print(jsonObj.toString());
	}

}

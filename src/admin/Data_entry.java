package admin;

import java.io.IOException;

import java.net.URISyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Tool.Properties_tool;

public class Data_entry extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if((int)(request.getSession().getAttribute("user_management_jurisdiction"))  == 1){
		try {
			String url = Data_entry.class.getResource("/admin/system_set.properties").toURI().getPath();
			Properties_tool pp = new Properties_tool(url);
			
			String ip = pp.getvalue("ip");
			request.setAttribute("ip", ip);
			
			request.getRequestDispatcher("/admin_data_entry.jsp").forward(request, response);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else{
			response.sendRedirect("/error-500.jsp");
		}
		
	}

}

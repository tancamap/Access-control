package admin;

import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import DB.DB_connection_pool;
import bean.Admin_inf;

public class Admin_jurisdiction_edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		int super_jurisdiction = (int)session.getAttribute("super_jurisdiction");
		
		if(super_jurisdiction == 1){
			String select_sql = "select * from admin_login";
			
			List<Admin_inf> admin_infs = new ArrayList<Admin_inf>();
			try {
				Connection conn = DB_connection_pool.data_pool.getConnection();
				PreparedStatement ps_select= conn.prepareStatement(select_sql);
				ResultSet rs = ps_select.executeQuery();
				
				while(rs.next()){
					Admin_inf admin = new Admin_inf();
					
					admin.setAdmin_account(rs.getString("admin_account"));
					admin.setAdmin_password(rs.getString("admin_password"));
					admin.setAdmin_nickname(rs.getString("admin_nickname"));
					admin.setSuper_jurisdiction(rs.getInt("super_jurisdiction"));
					admin.setBasic_jurisdiction(rs.getInt("basic_jurisdiction"));
					admin.setDevice_management_jurisdiction(rs.getInt("device_management_jurisdiction"));
					admin.setUser_management_jurisdiction(rs.getInt("user_management_jurisdiction"));
					
					admin_infs.add(admin);
				}
				
				rs.close();
				ps_select.close();
				conn.close();
				
				request.setAttribute("admin_infs", admin_infs);
				request.getRequestDispatcher("/admin_admin_jurisdiction_edit.jsp").forward(request, response);
				
			} catch (SQLException e) {
				
				response.sendRedirect("/error-500.jsp");
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}else{
			//没有权限
			response.sendRedirect("/error-500.jsp");
		}
		
	}

}

package admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import equipment.finger.Finger_tool.CLibrary;

@WebServlet("/Admin_fingerprint_on_off")
public class Admin_fingerprint_on_off extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		
		int on_off = Integer.parseInt(request.getParameter("on_off"));
		if(on_off==1){
			int ret = CLibrary.INSTANCE.sensorInit();
			if(ret != 0){
	    		System.out.println("设备初始化失败！");
	    	}
			int sensoropen = CLibrary.INSTANCE.sensorOpen(0);
			if(sensoropen == 0){
	    		CLibrary.INSTANCE.sensorFree();
	    		System.out.println("链接设备失败！");
	    	}
			System.out.println("指纹设备初始化成功");
			request.getServletContext().setAttribute("sensoropen", sensoropen);
		}else if(on_off==0){
			CLibrary.INSTANCE.sensorClose();   //此函数会关闭整个进程，，，设备关闭失败。
			System.out.println("指纹设备已关闭");
		}
		
		jsonObj.put("result", true);
		out.print(jsonObj.toString());
	}

}

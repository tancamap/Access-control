package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Clock;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import equipment.finger.Finger_col;

@WebServlet("/Finger_collect")
public class Finger_collect extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		String path = request.getServletContext().getRealPath("/finger");
		Clock clock = Clock.systemUTC();
		String finger_id = String.valueOf((int)(Math.random()*900000)+100000)+clock.millis();  //指纹编号
		
		boolean tf = new Finger_col().Finger_collect(finger_id,path+finger_id+".bmp", (int)request.getServletContext().getAttribute("sensoropen"));
		if(tf == true){
			jsonObj.put("result", true);
			jsonObj.put("finger_id",finger_id);
			jsonObj.put("user_fingerprint", "finger/"+finger_id+".bmp");
		}else{
			jsonObj.put("result", false);
		}
		out.print(jsonObj.toString());
	}

}

package admin;

import java.io.IOException;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import DB.DB_connection_pool;
public class Admin_login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		HttpSession session = request.getSession();
		
		String admin_account = request.getParameter("admin_account");
        String admin_password = request.getParameter("admin_password");	
		
        String select_sql = "select * from admin_login where admin_account = '"+admin_account+"' and admin_password = '"+admin_password+"'";
        
        try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_select= conn.prepareStatement(select_sql);
			ResultSet rs = ps_select.executeQuery();
			
			if(rs.next()){
				session.setAttribute("admin_account", admin_account);     //管理员账号
				session.setAttribute("admin_password", admin_password);      //管理员密码
				session.setAttribute("admin_nickname", rs.getString("admin_nickname"));    //管理员昵称
				session.setAttribute("super_jurisdiction", rs.getInt("super_jurisdiction"));     //管理员超级权限
				session.setAttribute("user_management_jurisdiction", rs.getInt("user_management_jurisdiction"));   //管理员用户管理权限
				session.setAttribute("device_management_jurisdiction", rs.getInt("device_management_jurisdiction"));  //管理员设备管理权限
				session.setAttribute("basic_jurisdiction", rs.getInt("basic_jurisdiction"));  //管理员设备管理权限
				
				//response.sendRedirect("/url");  ajax请求下servlet中是不会跳转的，同样情况下filter会跳转
				jsonObj.put("result", true);
			}else{
				//
				jsonObj.put("result", false);
			}
			
			
			rs.close();
			ps_select.close();
			conn.close();
		} catch (SQLException e) {
			jsonObj.put("result", false);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.print(jsonObj.toString());
	}

}

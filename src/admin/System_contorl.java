package admin;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/System_contorl")
public class System_contorl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getServletContext().getAttribute("Access_control") == null){
			request.getServletContext().setAttribute("Access_control", 0);
			bean.System_contorl.setAccess_control(0);
		}
		if(request.getServletContext().getAttribute("clock_control") == null){
		request.getServletContext().setAttribute("clock_control", 0);
		bean.System_contorl.setClock_control(0);
		}
		
		request.getRequestDispatcher("system_control.jsp").forward(request, response);
	}

}

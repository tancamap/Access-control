package admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Admin_jurisdiction_add extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		
		int super_jurisdiction = (int)session.getAttribute("super_jurisdiction");
		
		if(super_jurisdiction == 1){
				request.getRequestDispatcher("/admin_admin_jurisdiction_add.jsp").forward(request, response);
		}else{
			//没有权限
			response.sendRedirect("/error-500.jsp");
		}
		
	}

}
